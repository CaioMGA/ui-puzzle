# README #

This is a game that uses Unity's UI elements to create puzzles.
Can you solve them?
Can you SOLVE solve them?

### Game Description ###
The game is based on puzzle boxes and physical puzzles.
For each puzzle there is a Solution and a Master Solution.
The player is rewarded with a badge of an Easter Egg for each Master Solution.

Each puzzle's solution is can be found at its script as a comment at the begining of the file.
Reading them will damage your experience playing them.
Play first, read them later.