﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSound : MonoBehaviour {

	public AudioClip soundHover;
	public AudioClip soundClick;
	public AudioClip soundCorrect;
	public AudioClip soundError;
	public AudioClip soundVictory;

	private Button button { get { return GetComponent<Button>(); } }
	private AudioSource source { get { return GetComponent<AudioSource>(); } }

	float pitch = 0.8f;

	void Start () {
		gameObject.AddComponent<AudioSource>();
	source.playOnAwake = false;

	}
	
	public void PlayHover(){
		source.PlayOneShot(soundHover);
	}

	public void PlayClick(){
		source.PlayOneShot(soundClick);
	}

	public void PlayError(){
		source.PlayOneShot(soundError);
	}

	public void PlayCorrect(){
		source.PlayOneShot(soundCorrect);
	}

	public void PlayVictory(){
		source.PlayOneShot(soundVictory);
	}
}
