﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle_3 : MonoBehaviour {
	public GameObject victory_screen;
	public GameObject easterEgg;
	public GameObject [] buttons;
	Button [] btns;
	PanelsController panels;

	void Start(){
		panels = GetComponent<PanelsController>();
		int length = buttons.Length;
		btns = new Button[length];
		for(int i=0; i<length; i++){
			btns[i] = buttons[i].GetComponent<Button>();
		}
	}

	public void activate_button(int btn){
		btns[btn].interactable = true;
	}

	public void deactivate_button(int btn){
		btns[btn].interactable = false;
	}

	public void reset_puzzle(){
		foreach(Button b in btns ){
			b.interactable = false;
		}

		for(int i = panels.panels.Length - 1; i >= 0; i--){
			panels.panel_out(i);
		}
		victory_screen.SetActive(false);
	}

	public void win(){
		//you win
		victory_screen.SetActive(true);
	}

}
