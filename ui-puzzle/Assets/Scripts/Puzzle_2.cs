﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Puzzle_2 : MonoBehaviour {

	public GameObject [] btns;
	public GameObject victory_screen;
	public GameObject easterEgg;
	public Color lit_color;
	public ButtonSound sounds;

	Color unlit_color;

	public int step_index = 0;

	public bool solved = false;
	public bool youwin = false;
	bool easterEggFound = false;

	void Start(){
		PlayerPrefs.DeleteAll();
		solved = (PlayerPrefs.GetInt("stage2") == 1) ? true : false;
		easterEggFound = (PlayerPrefs.GetInt("easter2") == 1) ? true : false;
		unlit_color = btns[0].GetComponent<Image>().color;
	}

	void button_pressed(int btn){
		if(!youwin){
			if(!solved){
				switch(btn){
				case 2:
					if(step_index == 0){
						step_index++;
						sounds.PlayCorrect();
					} else {
						step_index = 0;
						sounds.PlayError();
					}
					break;
				case 1:
					if(step_index == 1){
						step_index++;
						sounds.PlayCorrect();
					} else {
						step_index = 0;
						sounds.PlayError();
					}
					break;
				case 0:
					if(step_index == 2){
						step_index++;
						sounds.PlayCorrect();
					} else {
						step_index = 0;
						sounds.PlayError();
					}
					break;
				}
			} else { //solved
				switch(btn){
				case 2:
					if(step_index == 0){
						step_index++;
						sounds.PlayCorrect();
					} else {
						step_index = 0;
						sounds.PlayError();
					}
					break;
				case 1:
					if(step_index >= 1 && step_index <=2){
						step_index++;
						sounds.PlayCorrect();
					} else {
						step_index = 0;
						sounds.PlayError();
					}
					break;
				case 0:
					if(step_index >= 3 && step_index <=5){
						step_index++;
						sounds.PlayCorrect();
					} else {
						step_index = 0;
						sounds.PlayError();
					}
					break;
				}
			}
		}
		update_lights();
	}

	void update_lights(){
		if(!solved){
			switch(step_index){
			case 0:
				// all off
				btns[0].GetComponent<Image>().color = unlit_color;
				btns[1].GetComponent<Image>().color = unlit_color;
				btns[2].GetComponent<Image>().color = unlit_color;
				break;
			case 1:
				//TOP and MID off
				//BOT ON
				btns[0].GetComponent<Image>().color = unlit_color;
				btns[1].GetComponent<Image>().color = unlit_color;
				btns[2].GetComponent<Image>().color = lit_color;
				break;
			case 2:
				//BOT and MID ON
				//TOP off
				btns[0].GetComponent<Image>().color = unlit_color;
				btns[1].GetComponent<Image>().color = lit_color;
				btns[2].GetComponent<Image>().color = lit_color;
				break;
			case 3:
				//ALL ON
				btns[0].GetComponent<Image>().color = lit_color;
				btns[1].GetComponent<Image>().color = lit_color;
				btns[2].GetComponent<Image>().color = lit_color;
				break;
			}
		} else {
			switch(step_index){
			case 0:
				//all off
				btns[0].GetComponent<Image>().color = unlit_color;
				btns[1].GetComponent<Image>().color = unlit_color;
				btns[2].GetComponent<Image>().color = unlit_color;
				break;
			case 1:
				//top and mid off
				// bot on
				btns[0].GetComponent<Image>().color = unlit_color;
				btns[1].GetComponent<Image>().color = unlit_color;
				btns[2].GetComponent<Image>().color = lit_color;
				break;
			
			case 3:
				//bot e mid on
				// top off
				btns[0].GetComponent<Image>().color = unlit_color;
				btns[1].GetComponent<Image>().color = lit_color;
				btns[2].GetComponent<Image>().color = lit_color;
				break;
			case 6:
				//all ON
				btns[0].GetComponent<Image>().color = lit_color;
				btns[1].GetComponent<Image>().color = lit_color;
				btns[2].GetComponent<Image>().color = lit_color;
				break;
			}
		}
	}

	void check_victory(){
		if(!youwin){
			if(!solved){
				if (step_index == 3){
					Debug.Log("Victory");
					youwin = true;
					solved = true;
					PlayerPrefs.SetInt("stage2", 1);
					victory_screen.SetActive(true);
					sounds.PlayVictory();
				}
			} else {
				if(step_index == 6){
					Debug.Log("Victory EasterEgg");
					youwin = true;
					PlayerPrefs.SetInt("easter2", 1);
					easterEggFound = true;
					easterEgg.SetActive(true);
					victory_screen.SetActive(true);
					sounds.PlayVictory();
				}
			}
		}
	}

	public void top(){
		button_pressed(0);
	}

	public void mid(){
		button_pressed(1);
	}

	public void bot(){
		button_pressed(2);
	}

	public void next(){
		SceneManager.LoadScene(3);
	}

	public void main_menu(){
		SceneManager.LoadScene(0);
	}

	public void reset_puzzle(){
		youwin = false;
		step_index = 0;
		update_lights();
		victory_screen.SetActive(false);
	}

	void Update(){
		check_victory();
	}
}
