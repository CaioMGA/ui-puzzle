﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Puzzle_1 : MonoBehaviour {
	/* Puzzle 1/10
	 * 
	 * Solution:
	 * Slide the sliders as they appear on the screen
	 * 
	 * Master Solution:
	 * After performing the solution described above, the board will
	 * start with all sliders visible.
	 * From Bottom to top, push the sliders one by one completely to the right
	 * */


	public GameObject [] sliders;
	public Slider [] handles;
	public GameObject victory_screen;
	public GameObject easterEgg;
	public Color lit_color;
	public ButtonSound sounds;

	Color unlit_color;

	bool solved = false;
	bool easterEggFound = false;
	bool youwin = false;
	bool win_registered = false;

	bool easter_step_0_solved = false;
	bool easter_step_1_solved = false;
	bool easter_step_2_solved = false;

	bool top_lit = false;
	bool mid_lit = false;
	bool bot_lit = false;

	public bool [] lit;

	void Start () {
		PlayerPrefs.DeleteAll();
		solved = (PlayerPrefs.GetInt("stage1") == 1) ? true : false;
		easterEggFound = (PlayerPrefs.GetInt("easter1") == 1) ? true : false;
		unlit_color = handles[0].colors.normalColor;
		lit = new bool[3]{false, false, false};
		reset_puzzle();
	}
	
	void Update () {
		update_lights();
		if(youwin){
			if(!win_registered){
				win_registered = true;
				sounds.PlayVictory();
				Debug.Log("You WIN!");
				PlayerPrefs.SetInt("stage1", 1);
				if(solved){
					easterEggFound = true;
					PlayerPrefs.SetInt("easter1", 1);
				} else {
					solved = true;
				}

				if(easterEggFound){
					easterEgg.SetActive(true);
				}
				victory_screen.SetActive(true);
			}

		} else if(!solved){
			if(handles[0].value == 1){
				sliders[1].SetActive(true);
				lit[0] = true;
			}
			if(handles[1].value == 1){
				sliders[2].SetActive(true);
				lit[1] = true;
			}

			if(handles[2].value == 1){
				lit[2] = true;
				youwin = true;
			}
		} else {
			if(handles[2].value == 1 ){
				if((handles[0].value == 1 || handles[1].value == 1) && !easter_step_0_solved){
					easter_step_0_solved = false;
				} else {
					easter_step_0_solved = true;
				}
			} else {
				easter_step_0_solved = false;
			}

			if(handles[1].value == 1){
				if((!easter_step_0_solved || handles[0].value == 1) && !easter_step_1_solved){
				} else {
					easter_step_1_solved = true;
				}
			} else {
				easter_step_1_solved = false;
			}

			if(handles[0].value == 1){
				if(easter_step_0_solved && easter_step_1_solved && !easter_step_2_solved ){
					easter_step_2_solved = true;
					youwin = true;

				} else {
					easter_step_2_solved = false;
				}
			} else {
				easter_step_2_solved = false;
			}
		}
	}

	public void reset_puzzle(){
		youwin = false;
		win_registered = false;
		victory_screen.SetActive(false);
		if(solved){
			foreach(GameObject go in sliders){
				go.SetActive(true);
			}
		} else {
			sliders[0].SetActive(true);
			sliders[1].SetActive(false);
			sliders[2].SetActive(false);
		}
		handles[0].value = 0;
		handles[1].value = 0;
		handles[2].value = 0;

		easter_step_0_solved = false;
		easter_step_1_solved = false;
		easter_step_2_solved = false;

		lit[0] = false;
		lit[1] = false;
		lit[2] = false;
	}

	void update_lights(){
		if(solved){
			if(easter_step_2_solved){
				if(!bot_lit){
					handles[0].fillRect.GetComponent<Image>().color = lit_color;
					bot_lit = true;
					sounds.PlayCorrect();
				}
			} else {
				if(bot_lit){
					handles[0].fillRect.GetComponent<Image>().color = unlit_color;
					bot_lit = false;
					sounds.PlayError();
				}
			}

			if(easter_step_1_solved){
				if(!mid_lit){
					handles[1].fillRect.GetComponent<Image>().color = lit_color;
					mid_lit = true;
					sounds.PlayCorrect();
				}
			} else {
				if(mid_lit){
					handles[1].fillRect.GetComponent<Image>().color = unlit_color;
					mid_lit = false;
					sounds.PlayError();
				}
			}

			if(easter_step_0_solved){
				if(!top_lit){
					handles[2].fillRect.GetComponent<Image>().color = lit_color;
					top_lit = true;
					sounds.PlayCorrect();
				}
			} else {
				if(top_lit){
					handles[2].fillRect.GetComponent<Image>().color = unlit_color;
					top_lit = false;
					sounds.PlayError();
				}
			}
		} else {
			if(lit[0]){
				if(!top_lit){
					handles[0].fillRect.GetComponent<Image>().color = lit_color;
					top_lit = true;
					sounds.PlayCorrect();
				}

			} else {
				if(top_lit){
					handles[0].fillRect.GetComponent<Image>().color = unlit_color;
					top_lit = false;
					sounds.PlayError();
				}
			}

			if(lit[1]){
				if(!mid_lit){
					handles[1].fillRect.GetComponent<Image>().color = lit_color;
					mid_lit = true;
					sounds.PlayCorrect();
				}
			} else {
				if(mid_lit){
					handles[1].fillRect.GetComponent<Image>().color = unlit_color;
					mid_lit = false;
					sounds.PlayError();
				}
			}

			if(lit[2]){
				if(!bot_lit){
					handles[2].fillRect.GetComponent<Image>().color = lit_color;
					bot_lit = true;
					sounds.PlayCorrect();
				}
			} else {
				if(bot_lit){
					handles[2].fillRect.GetComponent<Image>().color = unlit_color;
					bot_lit = false;
					sounds.PlayError();
				}
			}
		}
	}

	public void next(){
		SceneManager.LoadScene(2);
	}

	public void main_menu(){
		SceneManager.LoadScene(0);
	}
}
