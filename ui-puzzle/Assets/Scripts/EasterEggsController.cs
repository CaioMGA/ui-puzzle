﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasterEggsController : MonoBehaviour {

	public GameObject [] easterEggs;

	void Start () {
		for(int i = 0; i < 10; i++){
			int index = i +1;
			if(PlayerPrefs.GetInt("easter"+index.ToString()) == 1){
				easterEggs[i].SetActive(true);
			}
		}
	}
}
