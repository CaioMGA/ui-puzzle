﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public GameObject stageSelect;

	public void play(){
		stageSelect.SetActive(true);
	}

	public void about(){
		Debug.Log("ABOUT");
	}

	public void settings(){
		Debug.Log("SETTINGS");
	}

	public void open_stage(int stageNum){
		SceneManager.LoadScene(stageNum);
	}
}
