﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelsController : MonoBehaviour {

	public GameObject [] panels;
	Animator [] anims;

	void Start () {
		int panels_count = panels.Length;
		anims  = new Animator[panels_count];
		for(int i = 0; i< panels_count; i++){
			anims[i] = panels[i].GetComponent<Animator>();
		}
	}
	
	public void panel_in(int panel_index){
		anims[panel_index].SetTrigger("IN");
	}

	public void panel_out(int panel_index){
		anims[panel_index].SetTrigger("OUT");

	}
}
